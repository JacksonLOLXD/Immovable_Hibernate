/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.City;
import java.util.List;

/**
 *
 * @author JacksonLOLXD
 */
public interface cityDAO {
    
    public List<City> showCities();
    
    public void newCity(City city);
    
    public void updateCity(City city);
    public void deleteCity(int idCity);
}
