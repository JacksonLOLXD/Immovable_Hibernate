package DAO;

import Model.City;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class cityDAOImpl implements cityDAO{

    @Override
    public List<City> showCities() {
        List<City> listCities = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        
        String hql="FROM City";
        try{
            listCities = session.createQuery(hql).list();
            transaction.commit();
            session.close();
        } catch(Exception e){
            System.out.println(e.getMessage());
            transaction.rollback();
        }
        return listCities;
    }

    @Override
    public void newCity(City city) {
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(city);
            session.getTransaction().commit();
        }catch(Exception e){
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally{
            if(session != null){
                session.close();
            }
        }
    }

    @Override
    public void updateCity(City city) {
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(city);
            session.getTransaction().commit();
        }catch(Exception e){
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally{
            if(session != null){
                session.close();
            }
        }
    }

    @Override
    public void deleteCity(int idCity) {
        Transaction tns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            tns = session.beginTransaction();
            City city = (City) session.load(City.class, idCity);
            session.delete(city);
            session.getTransaction().commit();
        }catch(Exception e){
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally{
            if(session != null){
                session.close();
            }
        }
    }
    
}
