/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import DAO.cityDAO;
import DAO.cityDAOImpl;
import Model.City;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author JacksonLOLXD
 */
@Named(value = "cityBean")
@RequestScoped
public class cityBean {
    
    private List<City> list;
    private City city;
    
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
    
    public List<City> getList() {
        cityDAO cDAO = new cityDAOImpl();
        list = cDAO.showCities();
        return list;
    }
    
    public void newCity(){
        cityDAO cDAO = new cityDAOImpl();
        cDAO.newCity(city);
        city = new City();
    }
    
    public void udpateCity(){
        cityDAO cDAO = new cityDAOImpl();
        cDAO.updateCity(city);
        city = new City();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Correcto","El registro se actualizó satisfactoriamente"));
    }
    
    public void deleteCity(int idCity){
        cityDAO cDAO = new cityDAOImpl();
        cDAO.deleteCity(idCity);
        city = new City();
    }
    
    public cityBean(){
        city = new City();
    }
    
    public void clean(){
        city = new City();
    }

    

    
    
}
